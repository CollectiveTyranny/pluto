#pragma once

/**
 * @file   FileDataReader.h
 * @author Brigham Keys, Esq. (bkeys@bkeys.org)
 * @brief  Header for Plutos functionality for processing filedata 
 * used in encryption.
 *
 * Copyright (c) 2016 Brigham Keys, Esq.
 */

#include "IDataReader.h"
#include <fstream>
#include <iostream>

namespace plt {

  /**
   * \brief class used in processing the file data for the encryption
   * \details Used when the data that needs to be encrypted is stored
   * inside of a file.
   */
  class FileDataReader: public IDataReader<char> {

  public:

    /**
     * \brief Constructor for FileDataReader class
     * \details Takes in a filename that is to be processed
     * by pluto for encryption
     * \param fileName file that is to be processed
     */
    FileDataReader(const char* fileName);

    /**
     * \brief places the data inside of memory
     * \details Creates a pointer to chars that contains the contents of
     * your file
     * \return pointer to the characters inside of the file
     */
    char* toRAM();

    /**
     * \brief pushes data to source
     * \details Takes the size and the data and puts them into a file
     */
    void toSource(char* source, int size);

    /**
     * \brief get size of data
     * \details gets the character count for your file.
     * \return how many characters were counted in your file.
     */
    int getSize();

    ~FileDataReader();
  private:
    std::fstream file;
    char* ptr;
    int size;
  };
}
