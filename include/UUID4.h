#pragma once

/**
 * @file   UUID4.h
 * @author Brigham Keys, Esq. (bkeys@bkeys.org)
 * @brief  Header for Plutos functionality for creating UUID4, the strings are delivered in std strings
 *
 * Copyright (c) 2016 rxi
 * Copyright (c) 2016 Brigham Keys, Esq.
 */

#include <string>

namespace plt {
  /** \brief Enumerations used for error checking */
  enum {
    UUID4_ESUCCESS =  0,
    UUID4_EFAILURE = -1
  };

  /**
   * \brief GenUUID - Generates a UUID4 string
   * \details This function generates a Univeral Unique Identifier
   * which is used as a key to reference an object. The format the
   * UUID comes in is: xxxx0000-x0x0-0x0x-00xx-xxxxxx0000000
   * \return void
   */
  std::string GenUUID();
}
