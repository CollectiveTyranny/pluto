#pragma once

/**
 * @file   IDataReader.h
 * @author Brigham Keys, Esq. (bkeys@bkeys.org)
 * @brief  Header for Plutos functionality for processing data 
 * used in encryption.
 *
 * Copyright (c) 2016 Brigham Keys, Esq.
 */

#include "TeaCrypter.h"

namespace plt {
  /**
   * \brief Template type, input the data type that is to be encrypted
   * \details The type specified in the template is the one to be encrypted
   * the encryption is flexible and can handle any kind of data
   */
  template <class Type>
    class IDataReader {
  public:

    /**
     * \brief places the data inside of memory
     * \details Creates a pointer to chars that contains the contents of
     * your data
     * \return pointer to the data type you specified in the template
     */
    virtual Type* toRAM() = 0;

    /**
     * \brief pushes data to source
     * \details Takes the size and the data and puts them into the specified data type
     */
    virtual void toSource(Type* source, int size) = 0;

    /**
     * \brief get size of data
     * \return how large the data is
     */
    virtual int getSize() = 0;
  };
}
