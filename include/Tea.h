#pragma once
#include <stdint.h>
/**
 * \brief Do not use these functions
 */
namespace plt {
  void Encrypt (uint32_t* v, uint32_t* k); //!< Private encryption method
  void Decrypt (uint32_t* v, uint32_t* k); //!< Private decryption method
}
