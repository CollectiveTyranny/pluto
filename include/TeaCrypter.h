#pragma once
/**
 * @file   TeaCrypter.h
 * @author Brigham Keys, Esq. (bkeys@bkeys.org)
 * @brief  Header for Plutos functionality for Tiny Encryption. This algorithm
 * is not very secure but it will work for quick encryption.
 *
 * Copyright (c) 2016 Brigham Keys, Esq.
 */
#include <stdint.h>
namespace plt {
  /**
   * \brief Encryption functionality of Pluto
   * \details Uses the Tiny Encryption Algorithm
   * (https://en.wikipedia.org/wiki/Tiny_Encryption_Algorithm)
   * to encrypt data.
   */
  class TeaCrypter {
  public:
    /**
     * \brief Default constructor for TEA crypter
     * \details Constructor calls for a description of the data being
     * encrypted, such as:
     *   -# the data itself
     *   -# How large the data is
     *   -# Key used to decrypt the data
     * \param ptr data to be encrypted
     * \param sizeof_data Size of the data
     * \param key number used to decrypt the data
     */
    TeaCrypter(void *ptr, unsigned int sizeof_data, uint32_t *key);

    /**
     * \brief EncryptData - invoke the encryption of the data
     * \details This function will encrypt the data
     * \return void
     */
    void EncryptData();

    /**
     * \brief DecryptData - invoke the decryption of the data
     * \details This function will decrypt the data
     * \return void
     */
    void DecryptData();

    /**
     * \brief GetData - Pull the data that is going to be encrypted
     * \details This function will return the data that is going to be
     * encrypted by pluto. Mostly for debugging purposes.
     * \return void* the data that is stored in the TeaCrypter
     */
    void *GetData();

    /**
     * \brief GetMemoryErr - Retrieve errors by TeaCrypter
     * \details Used for debugging, this will let the user know
     * if any fatal errors involving memory have occured.
     * \return void
     */
    bool GetMemoryErr() const;

    ~TeaCrypter();
  private:
    uint32_t* inputData;
    uint32_t* key;
    unsigned int sizeof_inputData;
    unsigned int sizeof_keptData;
    bool memoryErr;
  };
}
