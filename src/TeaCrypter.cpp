#include "TeaCrypter.h"
#include "Tea.h"
#include <cstdlib>

plt::TeaCrypter::TeaCrypter(void* ptr, unsigned int sizeof_data, uint32_t* key): key(key), sizeof_inputData(sizeof_data) {
  uint32_t* uint32_t_ptr = static_cast<uint32_t*>(ptr);

  sizeof_keptData = sizeof_inputData + sizeof_inputData % 16;

  inputData = static_cast<uint32_t*>(malloc(sizeof_keptData));
  if ( inputData ) {
    for ( int i = 0, border = sizeof_inputData/4; i <= border; i++ ) {
      *(inputData + i) = *(uint32_t_ptr + i);
    }
    memoryErr = false;
  } else {
    memoryErr = true;
  }
}

void plt::TeaCrypter::EncryptData() {
  if ( !memoryErr ) {
    uint32_t* ptr_begin = inputData;
    uint32_t* ptr_end = ptr_begin + sizeof_keptData/4;

    for ( uint32_t* ptr = ptr_begin; ptr <= ptr_end; ptr+=2 ) {
      Encrypt(ptr, key);
    }
  }
}

void plt::TeaCrypter::DecryptData() {
  if ( !memoryErr ) {
    uint32_t* ptr_begin = inputData;
    uint32_t* ptr_end = ptr_begin + sizeof_keptData/4;

    for ( uint32_t* ptr = ptr_begin; ptr <= ptr_end; ptr+= 2 ) {
      Decrypt(ptr, key);
    }
  }
}

void* plt::TeaCrypter::GetData() {
  return static_cast<void*>(inputData);
}

bool plt::TeaCrypter::GetMemoryErr() const {
  return memoryErr;
}

plt::TeaCrypter::~TeaCrypter() {
  if ( !memoryErr ) {
    free(inputData);
  }
}
