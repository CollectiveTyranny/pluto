#!/bin/bash
cd build/
cmake -DCMAKE_TOOLCHAIN_FILE=../Toolchain-mingw32.cmake -DCMAKE_EXE_LINKER_FLAGS="-static -static-libgcc -static-libstdc++" ..
make VERBOSE=1
cd ..
