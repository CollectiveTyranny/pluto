Pluto contains the following functionaities:
- Running SHA1sums
- Using the Tiny Encryption algorithm
- Generating ID's with UUID version 4

SHA1
========

A simple header only SHA1 implementation in C++ (no dependencies). Based on the implementation in boost::uuid::details.

SHA1 Wikipedia Page: http://en.wikipedia.org/wiki/SHA-1

Usage
=====
Just include the header file to use in your code. There are no dependencies other than standard C++. 

```cpp
#include "pluto.h"

void testSHA1(const std::string& val) {
  sha1::SHA1 s;
	s.processBytes(val.c_str(), val.size());
	uint32_t digest[5];
	s.getDigest(digest);	
	char tmp[48];
	snprintf(tmp, 45, "%08x %08x %08x %08x %08x", digest[0], digest[1], digest[2], digest[3], digest[4]);
	std::cout<<"Calculated : (\""<<val<<"\") = "<<tmp<<std::endl;
}
```

There are also files in the example folder which shows the other uses of the Pluto library.

Pluto is free software, you can use it under the terms of the GNU General Public License, which should of been included with the software.

Copyright (c) 2012-22 SAURAV MOHAPATRA <mohaps@gmail.com> \n
Copyright (c) 2016-22 Brigham Keys, Esq. <bkeys@gnu.org>
